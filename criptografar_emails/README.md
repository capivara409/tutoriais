# Precisamos criptografar nossos e-mails!

> Você não precisa estar planejando um ataque terrorista para ter motivos para se comunicar de maneira segura,
> nem precisa ser hacker para poder fazê-lo.

A comunicação digital já faz parte completamente do nosso dia-a-dia, enquanto vivemos num mundo de ultra-vigilância.

Suas correspondências privadas devem permanecer privadas. Muitas pessoas como companheiros possessivos, patrões e Estados têm grande interesse em saber com quem você anda conversando sobre o que, e na maioria das vezes nem se trata de uma suspeita específica mas de uma vontade de controle generalizada.    

Antes que você me pergunte ""

As comunicações digitais já estão completamente inseridas no nosso dia-a-dia 

Quando enviamos uma carta pelo correio, por mais inofencivo que seja o que temos a dizer a nosso parente que mora longe, nós a colocamos dentro de um envelope  

## Por que?

## Como?

### Criptografia assimétrica

### Configurar um cliente de e-mail

## Na prática

Baixe a versão para seu sistema operacional do cliente de e-mails [Thunderbird no site oficial](https://www.mozilla.org/thunderbird/) e a instale.

Configure sua conta de e-mails no Thunderbird.

Instale o plugin [Enigmail](http://www.enigmail.net/) pelo Gerenciador de Extensões do Thunderbird.

Crie um par chaves pública/privada pelo Gerenciador de Chaves [OpenPGP](http://openpgp.org/about/)
